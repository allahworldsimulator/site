import 'bootstrap/dist/css/bootstrap.min.css';
import "bootswatch/dist/cyborg/bootstrap.min.css";
import './App.css';
import {AppContainer} from "./components/AppContainer";
import {HashRouter, Route, Switch} from "react-router-dom";
import {PopoutChat} from "./components/PopoutChat";

function App({config}) {
  return (
    <div className="App">
        <HashRouter>
            <Switch>
                <Route path="/popout-chat" component={PopoutChat} />
                <Route exact path="/:streamTitle?" >
                    <AppContainer config={config}/>
                </Route>

            </Switch>
        </HashRouter>
    </div>
  );
}

export default App;
