
export function AngelthumpStream({channel, className = "stream-iframe"}) {


    return (
        <iframe
            className={className}
            key={"AngelthumpStream" + channel}
            title="Angelthump Stream"
            frameBorder={0}
            allow={"autoplay; fullscreen"}
            allowFullScreen={true}
            scrolling={"no"}
            src={`https://player.angelthump.com/?channel=${channel}&autoplay=true`}
        />
    )
}
