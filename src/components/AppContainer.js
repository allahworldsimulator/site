import {SettingsContextProvider} from "../context/SettingsContext";
import {Layout} from "./Layout";

export function AppContainer({config}) {

    return (
        <SettingsContextProvider streams={config.streams} chats={[config.chat]}>
            <Layout/>
        </SettingsContextProvider>
    )
}
