
export function ChatContainer({activeChat, popout = false}) {

    return (<div className={"h-100 w-100 " + (popout ? '' : 'chat-container')}>
        {activeChat}
    </div>)

}
