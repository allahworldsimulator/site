import {MenuBar} from "./MenuBar";
import {ChatContainer} from "./ChatContainer";
import {useContext, useState} from "react";
import {SettingsContext} from "../context/SettingsContext";
import {ChatFactory} from "../service/ChatFactory";

export function ChatPane(){

    const { activeChat, popoutChat } = useContext(SettingsContext)
    const [visible] = useState(true)

    const chatComponent = ChatFactory(activeChat)

    if(visible) return (
        <div className={"h-100" + (popoutChat ? " d-none" : "")}>
            <MenuBar/>
            <ChatContainer activeChat={chatComponent}/>
        </div>
    )

    return <></>
}
