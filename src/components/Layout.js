import {Col, Container} from "react-bootstrap";
import {StreamPane} from "./StreamPane";
import {ChatPane} from "./ChatPane";
import Split from "react-split"
import {useContext} from "react";
import {SettingsContext} from "../context/SettingsContext";

export function Layout() {

    const {chatOnLeft, popoutChat} = useContext(SettingsContext)

    const streamColumn = <Col id={"stream-pane"} className={"h-100"}><StreamPane/></Col>

    const chatColumn = <Col id={"chat-pane"} className={"h-100" + (popoutChat ? " d-none" : "")}><ChatPane/></Col>

    const createSplit = (left, right, reverseSizes) => (<Split
        className={"row h-100 no-gutters vw-100"}
        elementStyle={function (dimension, size, gutterSize) {
            return {'flex-basis': `calc(${size}% - ${gutterSize}px)`}
        }}
        gutterStyle={function (dimension, gutterSize) {
            return {'flex-basis': gutterSize + 'px'}
        }}
        gutter={(index, direction) => {
            const gutter = document.createElement('div')
            gutter.className = `split-gutter split-gutter-${direction} p-0 m-0`
            return gutter
        }}
        onDragEnd={(sizes) => window.localStorage.setItem("splitSizes", reverseSizes ? sizes.reverse() : sizes)}
        sizes={(() => {
            const localStorageValue = window.localStorage.getItem("splitSizes")
            if (!localStorageValue) return reverseSizes ? [25, 75] : [75, 25]
            const parsed =  localStorageValue.split(',').map(it => +it)
            return reverseSizes ? parsed.reverse() : parsed
        })()}
        minSize={200}
        gutterSize={4}
        cursor={'col-resize'}
    >
        {left}
        {right}
    </Split>)

    const split = chatOnLeft ? createSplit(chatColumn, streamColumn, true) : createSplit(streamColumn, chatColumn, false)

    return (
        <Container fluid className={"vh-100 p-0 m-0"}>
            {split}
        </Container>
    )
}
