import {Dropdown, Form} from "react-bootstrap";
import {useContext} from "react";
import {SettingsContext} from "../context/SettingsContext";

export function MenuBar() {

    const {
        inverted,
        setInverted,
        streams,
        activeStream,
        setActiveStream,
        chatOnLeft,
        activeChat,
        setChatOnLeft,
        popoutChat,
        setPopoutChat,
    } = useContext(SettingsContext)

    const streamItems = streams.map((stream) =>
        <Dropdown.Item className={"w-100"}
                        key={stream.title}
                       active={activeStream === stream}
                       onClick={() => {
                           setActiveStream(stream)
                       }}>{stream.title}</Dropdown.Item>
    )

    const openPopup = () => {
        try {
            const popup = window.open(
                "/#/popout-chat?chatConfig=" + encodeURIComponent(JSON.stringify(activeChat)),
                "_blank",
                "toolbar = 0, scrollbars = 0, statusbar = 0, menubar = 0, height=700, width=400"
            )

            setPopoutChat(true)
            popup.onbeforeunload= () => {
                setPopoutChat(false)
            }
        } catch (e) {
            console.log("error opening popup " + e)
        }

    }

    return (
        <div className={"d-flex w-100 justify-content-end"}>
            <Dropdown className={"flex-grow-1 w-100"}>
                <Dropdown.Toggle id="select-stream-dropdown-toggle" variant={"secondary"}
                                 className={"menu-bar-button w-100"}>
                    Select Stream
                </Dropdown.Toggle>
                <Dropdown.Menu className={"w-100"}>
                    {streamItems}
                </Dropdown.Menu>
            </Dropdown>
            <Dropdown id={"settings-button"}>
                <Dropdown.Toggle id="select-stream-dropdown-toggle" variant={"secondary"}
                                 className={"menu-bar-button w-100"}>
                    <i className="bi bi-gear-fill" role="img" aria-label="GitHub"/>
                </Dropdown.Toggle>
                <Dropdown.Menu>
                    <Form>
                        <Dropdown.Item onClick={() => setInverted(!inverted)}>
                            <Form.Check type={"checkbox"} checked={inverted} label={"Invert player color"}/>
                        </Dropdown.Item>
                        <Dropdown.Item className={"d-none d-md-block"} onClick={() => setChatOnLeft(!chatOnLeft)}>
                            <Form.Check type={"checkbox"} checked={chatOnLeft} label={"Chat on left"}/>
                        </Dropdown.Item>
                        <Dropdown.Item className={"d-none d-md-block"} onClick={() => openPopup()}>
                            <Form.Check type={"checkbox"} checked={popoutChat} label={"Popout chat"}/>
                        </Dropdown.Item>
                    </Form>
                </Dropdown.Menu>
            </Dropdown>
        </div>


    )

}
