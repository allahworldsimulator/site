import {ChatFactory} from "../service/ChatFactory";
import {ChatContainer} from "./ChatContainer";


export function PopoutChat({location}) {
    const config = generateConfig(location.search)
    const component = ChatFactory(config)

    return (
        <div className={'vh-100 vw-100'}><ChatContainer popout={true} activeChat={component}/></div>
    )
}

function generateConfig(queryString) {
    const urlParams = new URLSearchParams(queryString);
    const encodedConfig = urlParams.get('chatConfig')
    const decodedConfig = decodeURIComponent(encodedConfig)

    return JSON.parse(decodedConfig)
}