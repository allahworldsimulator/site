export function StreamContainer({activeStream, className}) {

    return (
        <div
            className={"stream-container-inner " + className}
        >
            {activeStream}
        </div>
    )

}
