import {StreamContainer} from "./StreamContainer";
import {useContext} from "react";
import {SettingsContext} from "../context/SettingsContext";
import {StreamFactory} from "../service/StreamFactory";


export function StreamPane() {

    const { activeStream, inverted } = useContext(SettingsContext)

    const streamComponent = StreamFactory(activeStream)

    return <StreamContainer className={inverted ? 'color-inverted' : ''} activeStream={streamComponent} />
}
