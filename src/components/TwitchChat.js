export function TwitchChat({channel, className = "chat-iframe w-100"}) {

    return (
        <iframe
            id={"twitch-chat"}
            key={"TwitchChat" + channel}
            className={className}
            title="Twitch Chat"
            frameBorder={0}
            src={`https://twitch.tv/embed/${channel}/chat?darkpopout&parent=${window.location.hostname}`}
        />
    )
}
