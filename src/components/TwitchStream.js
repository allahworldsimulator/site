export function TwitchStream({channel, className = "stream-iframe"}) {

    return (
        <iframe
            className={className}
            key={"TwitchStream" + channel}
            title="Twitch Chat"
            frameBorder={0}
            allow={"autoplay; fullscreen"}
            allowFullScreen={true}
            scrolling={"no"}
            src={`https://player.twitch.tv/?channel=${channel}&parent=${window.location.hostname}&autoplay=true`}
        />
    )
}
