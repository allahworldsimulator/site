import {createContext, useState} from "react";
import {useParams} from "react-router";

export const SettingsContext = createContext({
    activeStream: null,
    inverted: false
})

const getInitialStream = (streams, streamTitle) => {

    if(streamTitle) {
        const found = streams.find(stream => stream.title === streamTitle)

        if (found) return found
    }

    return streams[0]
}

export const SettingsContextProvider = ({streams, chats, children}) => {
    const [activeStream, setActiveStreamInternal] = useState(null)
    const [activeChat, setActiveChat] = useState(null)
    const [inverted, setInverted] = useState(false)
    const [chatOnLeft, setChatOnLeftInternal] = useState(null)
    const [popoutChat, setPopoutChat] = useState(false)

    const { streamTitle } = useParams()

    if (activeStream == null) setActiveStreamInternal(getInitialStream(streams, streamTitle))

    if (activeChat == null) setActiveChat(chats[0])


    if (chatOnLeft == null) {
        const onLeft = window.localStorage.getItem("chatOnLeft") !== null ? JSON.parse(window
            .localStorage.getItem("chatOnLeft")) : false
        setChatOnLeftInternal(onLeft)
    }

    const setActiveStream = (stream) => {
        window.history.replaceState(null, null, `#/${stream.title}`)
        setActiveStreamInternal(stream)
    }

    const setChatOnLeft = (onLeft) => {
        window.localStorage.setItem("chatOnLeft", onLeft)
        setChatOnLeftInternal(onLeft)
    }

    return (
        <SettingsContext.Provider value={{
            activeStream,
            setActiveStream,
            activeChat,
            inverted,
            setInverted,
            streams,
            chatOnLeft,
            setChatOnLeft,
            popoutChat,
            setPopoutChat
        }}>
            {children}
        </SettingsContext.Provider>
    )
}
