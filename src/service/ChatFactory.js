import {TwitchChat} from "../components/TwitchChat";

function TwitchFactory(config) {
    return (<TwitchChat channel={config.channel}/>)
}

function ErrorFactory() {
    return <div><h1 style={{color:"red"}}>CHAT MISSING</h1></div>
}

const factories = {
    'TwitchChat': TwitchFactory
}

const cache = new WeakMap()

export const ChatFactory = (config) => {
    if(config == null) return null

    const factory = factories[config.type] ?? ErrorFactory
    const stream =  factory(config.config)
    cache.set(config, stream)

    return cache.get(config)
}
