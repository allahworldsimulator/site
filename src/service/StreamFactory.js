import {TwitchStream} from "../components/TwitchStream";
import {AngelthumpStream} from "../components/AngelthumpStream";

function TwitchFactory(config) {
    return (<TwitchStream channel={config.channel}/>)
}

function AngelthumpFactory(config) {
    return (<AngelthumpStream channel={config.channel} />)
}

const factories = {
    'AngelthumpStream': AngelthumpFactory,
    'TwitchStream': TwitchFactory
}

const cache = new WeakMap()

export const StreamFactory = (config) => {
    if(config == null) return null

    const factory = factories[config.type]
    const stream =  factory(config.config)
    cache.set(config, stream)

    return cache.get(config)
}
