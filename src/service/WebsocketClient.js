import Sarus from "@anephenix/sarus";
import {toast} from "react-toastify";

export class WebsocketClient {

    #commandListeners = []
    #presenceListeners = []
    #hasDisconnected = false

    #websocket

    constructor() {
        console.log("constructing websocket listener")
    }

    start() {
        console.log("starting websocket listener")

        this.#websocket = new Sarus({
            url: `ws://localhost:8080/commands?site=${window.location.hostname}`,
            eventListeners: {
                close: [this.notifyDisconnect.bind(this)],
                open: [this.notifyReconnect.bind(this)],
                message: [this.handleMessage.bind(this)]
            }
        })
    }

    notifyDisconnect() {
        console.warn("lost connection")
        if(!this.#hasDisconnected) {
            //pretty ugly calling notifications from in here but oh well
            toast.error("Lost connection to server.")
            this.#hasDisconnected = true
        }
    }

    notifyReconnect() {
        console.log("connected to server")
        if(this.#hasDisconnected) {
            toast.success("Connected to server.")
            this.#hasDisconnected = false
        }
    }

    handleMessage(message) {
        let parsed
        try {
            parsed = JSON.parse(message.data)
        } catch(e) {
            console.error("Error parsing message " + message)
        }

        if(parsed.type && parsed.type === 'notification') {
            toast(parsed.content.text)
        } else if(parsed.type && parsed.type === 'command') {
            this.handleCommandMessage(parsed)
        } else if(parsed.type && parsed.type === 'presence') {
            this.handlePresenceMessage(parsed)
        }
    }


    handleCommandMessage(parsedMessage) {
        this.#commandListeners.forEach(it => it(parsedMessage))
    }

    handlePresenceMessage(parsedMessage) {
        this.#presenceListeners.forEach(it => it(parsedMessage))
    }

    registerCommandListener(callback) {
        this.#commandListeners.push(callback)
    }

    registerPresenceListener(callback) {
        this.#presenceListeners.push(callback)
    }


}
